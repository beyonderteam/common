export interface ModalProps {
    inClassname?: string;
    outClassname?: string;
    onClose?: any;
}

export interface ModalState {
    isOpen: boolean;
}

export interface ModalBehavior {
    close(): void;
}