# @beyonder/common
Abstraction des composants Beyonder (interfaces TypeScript) et feuilles de styles (fichiers SASS).
Ce projet est en dépendances de @beyonder/angular-components, @beyonder/react-components et peut-être/bientôt @beyonder/vue-components.

## Phase de développement
Ce projet ne requiert aucun npm install, ni transpilation. Aucun besoin de générer de fichier JS, il est utilisé uniquement dans le but d'utiliser les fichiers SCSS et TS qu'il contient.
